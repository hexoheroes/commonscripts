namespace Common.HexField.HexPosition
{
    public class PositionIntComparator
    {
        private readonly PositionInt _pos1;
        private readonly PositionInt _pos2;

        public PositionIntComparator(PositionInt pos1, PositionInt pos2)
        {
            _pos1 = pos1;
            _pos2 = pos2;
        }
        
        public bool Equals()
        {
            return _pos1.X == _pos2.X && _pos1.Y == _pos2.Y && _pos1.Z == _pos2.Z;
        }
    }
}
