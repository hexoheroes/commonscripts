using UnityEngine;

namespace Common.HexField.HexPosition
{
    public static class PositionFloatUtils
    {
        public static PositionFloat Sum(PositionFloat positionA, PositionFloat positionB)
        {
            return new PositionFloat(positionA.X + positionB.X, positionA.Y + positionB.Y, positionA.Z + positionB.Z);
        }
        
        public static PositionInt ToIntPosition(this PositionFloat position)
        {
            var roundedX = Mathf.RoundToInt(position.X);
            var roundedY = Mathf.RoundToInt(position.Y);
            var roundedZ = Mathf.RoundToInt(position.Z);

            var xDiff = Mathf.Abs(roundedX - position.X);
            var yDiff = Mathf.Abs(roundedY - position.Y);
            var zDiff = Mathf.Abs(roundedZ - position.Z);

            if (xDiff > yDiff && xDiff > zDiff)
            {
                roundedX = -roundedY - roundedZ;
            }
            else if (yDiff > zDiff)
            {
                roundedY = -roundedX - roundedZ;
            }
            else
            {
                roundedZ = -roundedX - roundedY;
            }

            return new PositionInt(roundedX, roundedY, roundedZ);
        }
    }
}
