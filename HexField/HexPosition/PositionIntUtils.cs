using System;
using UnityEngine;

namespace Common.HexField.HexPosition
{
    public static class PositionIntUtils
    {
        public static PositionInt Sum(PositionInt positionA, PositionInt positionB)
        {
            return new PositionInt(positionA.X + positionB.X, positionA.Y + positionB.Y, positionA.Z + positionB.Z);
        }

        public static int Distance(PositionInt startPosition, PositionInt endPosition)
        {
            return (Mathf.Abs(startPosition.X - endPosition.X) + Mathf.Abs(startPosition.Y - endPosition.Y) + Mathf.Abs(startPosition.Z - endPosition.Z)) / 2;
        }

        public static PositionFloat Lerp(PositionInt startPosition, PositionInt endPosition, float t)
        {
            var x = Mathf.Lerp(startPosition.X, endPosition.X, t);
            var y = Mathf.Lerp(startPosition.Y, endPosition.Y, t);
            var z = Mathf.Lerp(startPosition.Z, endPosition.Z, t);

            return new PositionFloat(x, y, z);
        }
        
        public static PositionInt GetNeighborPointyGridPosition(this PositionInt position, int direction)
        {
            return direction switch
            {
                1 => Sum(position, new PositionInt(1, 0, -1)),
                2 => Sum(position, new PositionInt(1, -1, 0)),
                3 => Sum(position, new PositionInt(0, -1, 1)),
                4 => Sum(position, new PositionInt(-1, 0, 1)),
                5 => Sum(position, new PositionInt(-1, 1, 0)),
                6 => Sum(position, new PositionInt(0, 1, -1)),
                _ => throw new ArgumentOutOfRangeException(nameof(direction), $"Neighbors range should be from 1 to 6. Actual value is {direction}")
            };
        }
    }
}
