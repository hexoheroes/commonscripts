using UnityEngine;

namespace Common.HexField.HexGridObject
{
    public class GridObject : MonoBehaviour
    {
        protected Transform Transform;

        private void Awake()
        {
            Transform = gameObject.GetComponent<Transform>();
        }

        public override string ToString()
        {
            return $"{name}";
        }

        public Vector3 GetPosition()
        {
            return Transform.position;
        }
    }
}
