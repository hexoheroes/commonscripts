using System.Collections.Generic;
using System.Linq;
using Common.Utils;
using Common.HexField.Exceptions;
using Common.HexField.HexGridObject;
using Common.HexField.HexPosition;
using UnityEngine;

namespace Common.HexField.HexGrid
{
    public class PointyGridCreator
    {
        private float _calculatedGridObjectsDistance;
        private readonly IEnumerable<GridObject> _gridObjects;
        private readonly Grid _grid;

        public PointyGridCreator(IEnumerable<GridObject> gridObjects)
        {
            _gridObjects = gridObjects;
            _grid = new Grid();
        }

        public Grid Create()
        {
            AddGridObjects(_gridObjects);
            return _grid;
        }
        
        private void AddGridObjects(IEnumerable<GridObject> gridObjects)
        {
            var gridObjectsList = gridObjects.ToList();
            var tempList = new List<GridObject>(gridObjectsList);
            
            for (var i = 0; i < gridObjectsList.Count; i++)
            {
                switch (_grid.Count())
                {
                    case 0:
                    {
                        var gridObject = tempList.PopAt(0);
                        _grid.Add(gridObject, new PositionInt(0, 0, 0));
                        break;
                    }
                    case 1:
                    {
                        _calculatedGridObjectsDistance = (from hexObjectsPair in tempList.GetAllPairs()
                            select Vector3.Distance(hexObjectsPair.Element1.GetPosition(), hexObjectsPair.Element2.GetPosition())).Min();
        
                        var closestGridObject = FindClosestNewGridObjectToGrid(tempList);
                        tempList.Remove(closestGridObject);
                        
                        var existedGridObject = _grid.FindGridObjectByPositionEx(new PositionInt(0, 0, 0));
                        _calculatedGridObjectsDistance = Vector3.Distance(existedGridObject.GetPosition(), closestGridObject.GetPosition());

                        var existedGridObjectPosition = _grid.FindGridObjectPositionEx(existedGridObject);
                        var newGridObjectPosition = GridUtils.CalcNewGridObjectPointyPosition(closestGridObject, existedGridObject, existedGridObjectPosition);
                        _grid.Add(closestGridObject, newGridObjectPosition);
                        break;
                    }
                    default:
                    {
                        var closestGridObject = FindClosestNewGridObjectToGrid(tempList);
                        tempList.Remove(closestGridObject);
                        
                        var nearestGridObject = FindClosestGridObjectToNewGridObject(closestGridObject);
                        var nearestGridObjectPosition = _grid.FindGridObjectPositionEx(nearestGridObject);
                        var newGridObjectPosition = GridUtils.CalcNewGridObjectPointyPosition(closestGridObject, nearestGridObject, nearestGridObjectPosition);
                        _grid.Add(closestGridObject, newGridObjectPosition);
                        break;
                    }
                }
            }
        }
        
        private GridObject FindClosestNewGridObjectToGrid(IList<GridObject> newGridObjects)
        {
            var (_, element2) = LinqUtils.GetAllPairs(_grid.GetObjects(), newGridObjects)
                                         .FirstOrDefault(pair => GridUtils.DistanceBetweenGridObjectEqual(pair.Element1, pair.Element2, _calculatedGridObjectsDistance));
            return element2 ? element2 : throw new GridDifferentDistanceException();
        }
        
        private GridObject FindClosestGridObjectToNewGridObject(GridObject newGridObject)
        {
            var hex = _grid.GetObjects().FirstOrDefault(gridObject => GridUtils.DistanceBetweenGridObjectEqual(gridObject, newGridObject, _calculatedGridObjectsDistance));
            return hex ? hex : throw new GridDifferentDistanceException();
        }
    }
}
