using System;
using System.Runtime.Serialization;
using Common.HexField.HexGridObject;
using Common.HexField.HexPosition;

namespace Common.HexField.Exceptions
{
    [Serializable]
    public class GridObjectMissingException : Exception
    {
        public GridObjectMissingException(PositionInt position) : base($"Grid object with position [{position}] is absent")
        {

        }
        
        public GridObjectMissingException(GridObject obj) : base($"Grid object [{obj}] is absent")
        {

        }

        public GridObjectMissingException(string message) : base(message)
        {

        }

        public GridObjectMissingException(string message, Exception inner) : base(message, inner)
        {

        }
        
        protected GridObjectMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}
