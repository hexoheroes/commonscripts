using System;
using System.Runtime.Serialization;

namespace Common.HexField.Exceptions
{
    [Serializable]
    public class GridDifferentDistanceException : Exception
    {
        public GridDifferentDistanceException() : base("Distance between elements are different")
        { 

        }

        public GridDifferentDistanceException(string message) : base(message)
        { 

        }

        public GridDifferentDistanceException(string message, Exception inner) : base(message, inner)
        {

        }
        
        protected GridDifferentDistanceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}
