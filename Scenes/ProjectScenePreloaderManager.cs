using UnityEditor;
using UnityEngine;

namespace Common.Scenes
{
    public class ProjectScenePreloaderManager : ProjectSceneManager
    {
        [SerializeField] private SceneAsset _nextScene;
        
        private void Start()
        {
            StartCoroutine(LoadNextSceneWithBeforeStep(PreloaderAction, _nextScene));
        }

        protected virtual void PreloaderAction()
        {
            
        }
    }
}
