using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.Scenes
{
    public class ProjectSceneManager : MonoBehaviour
    {
        public bool IsFullyLoaded { get; protected set; }

        protected IEnumerator LoadNextSceneWithBeforeStep(Action step, SceneAsset nextScene)
        {
            step();

            var loadSceneAsync = SceneManager.LoadSceneAsync(nextScene.name);
            
            while (!loadSceneAsync.isDone)
            {
                yield return null;
            }
        }
    }
}
