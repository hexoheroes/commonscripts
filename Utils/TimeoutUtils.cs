using System.Collections;
using UnityEngine;

namespace Common.Utils
{
    public static class TimeoutUtils
    {
        public static IEnumerator Timeout(float seconds)
        {
            yield return new WaitForSeconds(seconds);
        }
    }
}