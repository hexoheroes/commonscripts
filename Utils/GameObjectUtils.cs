﻿using UnityEngine;

namespace Common.Utils
{
    public static class GameObjectUtils
    {
        public static T Instantiate<T>(T original, Transform parent = null, bool worldPositionStays = false) where T : Object
        {
            var obj = Object.Instantiate(original, parent, worldPositionStays);
            obj.name = GameObjectNameUtils.Instance().ProcessName(obj.name);
            return obj;
        }
        
        public static T InstantiateNotActive<T>(T original, Transform parent = null, bool worldPositionStays = false) where T : Object
        {
            var obj = Instantiate(original, parent, worldPositionStays);
            var monoBehaviour = obj as MonoBehaviour;
            monoBehaviour.gameObject.SetActive(false);
            return obj;
        }

        public static void DestroyChildren(this Transform root)
        {
            foreach (Transform child in root) {
                GameObject.Destroy(child.gameObject);
            }
        }
    }
}
