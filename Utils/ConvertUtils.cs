using System;

namespace Common.Utils
{
    public static class ConvertUtils
    {
        public static T GetValue<T>(this object value)
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}