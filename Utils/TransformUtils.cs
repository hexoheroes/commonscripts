using UnityEngine;

namespace Common.Utils
{
    public static class TransformUtils
    {
        public static void SetXPosition(this Transform transform, float xPos)
        {
            transform.position = transform.position.SetX(xPos);
        }
    }
}