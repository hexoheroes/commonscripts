using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine.SceneManagement;

namespace Common.Utils
{
    public static class AssetUtils
    {
        public static List<string> GetBuiltScenePaths()
        {
            var guids = AssetDatabase.FindAssets("t:SceneAsset", new[] {GlobalVariables.PathToSceneAssets}).ToList();
            var scenePaths = guids.Select(AssetDatabase.GUIDToAssetPath).ToList();
            scenePaths = scenePaths.OrderBy(SceneUtility.GetBuildIndexByScenePath).ToList();
            scenePaths.RemoveAll(path => SceneUtility.GetBuildIndexByScenePath(path) == -1);
            return scenePaths;
        }
        
        public static List<string> GetAllScenePaths()
        {
            var guids = AssetDatabase.FindAssets("t:SceneAsset", new[] {GlobalVariables.PathToSceneAssets}).ToList();
            var scenePaths = guids.Select(AssetDatabase.GUIDToAssetPath).ToList();
            scenePaths = scenePaths.OrderBy(SceneUtility.GetBuildIndexByScenePath).ToList();
            return scenePaths;
        }
    }
}