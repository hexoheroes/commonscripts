﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Utils
{
    public static class TextAssetUtils
    {
        public static List<string> ReadLines(this TextAsset textAsset)
        {
            var text = textAsset.text;
            return text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
