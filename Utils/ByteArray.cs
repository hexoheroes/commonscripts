using System.Text;

namespace Common.Utils
{
    public static class ByteArrayUtils
    {
        public static string GetString(this byte[] array)
        {
            return Encoding.UTF8.GetString(array);
        }
    }
}