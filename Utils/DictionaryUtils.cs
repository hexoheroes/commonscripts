using System.Collections.Generic;

namespace Common.Utils
{
    public static class DictionaryUtils
    {
        public static void AddToListValue<T, TU>(this Dictionary<T, List<TU>> dict, T key, TU value)
        {
            if (dict.TryGetValue(key, out var items))
            {
                items.Add(value);
            }
            else
            {
                dict.Add(key, new List<TU> { value });
            }
        }
    }
}
