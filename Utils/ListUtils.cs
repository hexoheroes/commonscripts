using System;
using System.Collections.Generic;
using System.Linq;

namespace Common.Utils
{
    public static class ListUtils
    {
        public static T GetElementByTypeName<T>(this List<T> list, string typeName)
        {
            return list.Find(el => el.GetType().Name.Equals(typeName));
        }

        public static T GetElementByType<T>(this List<T> list, Type type)
        {
            return list.Find(el => el.GetType() == type);
        }

        public static T PopAt<T>(this List<T> list, int index)
        {
            var element = list[index];
            list.RemoveAt(index);
            return element;
        }

        public static List<T> InitList<T>(int count, T value)
        {
            var list = new List<T>();
            for (var i = 0; i < count; i++)
            {
                list.Add(value);
            }

            return list;
        }

        public static bool Equals<T>(List<T> list1, List<T> list2)
        {
            var firstNotSecond = list1.Except(list2);
            var secondNotFirst = list2.Except(list1);
            return list1.Count == list2.Count && !firstNotSecond.Any() && !secondNotFirst.Any();
        }
        
        public static bool EqualsWithOrder<T>(List<T> list1, List<T> list2)
        {
            return list1.Count == list2.Count && list1.Intersect(list2).SequenceEqual(list2);
        }
        
        public static bool IsEmpty<T>(this List<T> list)
        {
            if (list == null) {
                return true;
            }
 
            return !list.Any();
        }
        
        public static IEnumerable<int> IndexesOf<T>(this List<T> list, T value)
        {
            return Enumerable.Range(0, list.Count).Where(i => list[i].Equals(value)).ToList();
        }

        public static IEnumerable<int> IndexesOf<T>(this List<T> list, List<T> values)
        {
            return Enumerable.Range(0, list.Count).Where(i => values.Contains(list[i])).ToList();
        }

        public static void RemoveLast<T>(this List<T> list)
        {
            if (list.IsEmpty()) return;
            list.RemoveAt(list.Count - 1);
        }
    }
}
