using System.Collections.Generic;
using System.Linq;

namespace Common.Utils
{
    public static class EnumerableUtils
    {
        public static T Random<T>(this IEnumerable<T> enumerable)
        {
            var list = enumerable.ToList();
            var random = new System.Random();
            var index = random.Next(list.Count);
            return list.ElementAt(index);
        }
    }
}
