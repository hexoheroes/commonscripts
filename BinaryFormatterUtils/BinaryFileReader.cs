using System;
using System.Runtime.Serialization.Formatters.Binary;
using Common.BinaryFormatterUtils.Exceptions;
using Common.Utils;
using Unity.Plastic.Newtonsoft.Json;

namespace Common.BinaryFormatterUtils
{
    public class BinaryFileReader<T>
    {
        private readonly BinaryFile<T> _file;

        public BinaryFileReader(BinaryFile<T> file)
        {
            _file = file;
        }
        
        public T ReadBinary()
        {
            if(_file.Exists())
            {
                try
                {
                    var bf = new BinaryFormatter();
                    var fileStream = _file.Open();
                    var binaryJson = (string) bf.Deserialize(fileStream);
                    fileStream.Close();
                    var json = binaryJson.ToStringFromBase64();
                    var obj = JsonConvert.DeserializeObject<T>(json);
                    return obj.GetValue<T>();
                }
                catch (Exception e)
                {
                    throw new BadValueException("", e);
                }
            }
            else
            {
                throw new BadValueException();
            }
        }
    }
}
