using System;
using System.Runtime.Serialization;

namespace Common.BinaryFormatterUtils.Exceptions
{
    [Serializable]
    public class BadValueException : Exception
    {
        public BadValueException() : base("Bad value")
        { 

        }

        public BadValueException(string message) : base(message)
        { 

        }

        public BadValueException(string message, Exception inner) : base(message, inner)
        {

        }
        
        protected BadValueException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}