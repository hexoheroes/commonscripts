using System;
using System.Runtime.Serialization;

namespace Common.PlayerPrefsUtils.Exceptions
{
    [Serializable]
    public class BadValueTypeException : Exception
    {
        public BadValueTypeException() : base("Value has bad type")
        { 

        }

        public BadValueTypeException(string message) : base(message)
        { 

        }

        public BadValueTypeException(string message, Exception inner) : base(message, inner)
        {

        }
        
        protected BadValueTypeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}
