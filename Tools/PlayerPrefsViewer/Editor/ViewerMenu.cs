using UnityEngine;
using UnityEditor;

namespace Common.Tools.PlayerPrefsViewer.Editor
{
    public class ViewerMenu : EditorWindow
    {
        [MenuItem("ToolsBox/PlayerPrefsViewer")]
        private static void Init()
        {
            System.Diagnostics.Process.Start(Application.dataPath + "/Scripts/Common/Tools/PlayerPrefsViewer/PlayerPrefsTool.exe", $"{Application.companyName}\\{Application.productName}");
        }
    }
}
