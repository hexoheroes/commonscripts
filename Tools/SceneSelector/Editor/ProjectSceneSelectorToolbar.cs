using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.Tools.ToolbarExtender;
using Common.UI.Toolbar.Elements;
using Common.Utils;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.Tools.SceneSelector.Editor
{
    [InitializeOnLoad]
    public static class ProjectSceneSelectorToolbar
    {
        static ProjectSceneSelectorToolbar()
        {
            var toolbar = new ProjectSceneSelectorToolbarImpl();
            ToolbarExtender.ToolbarExtender.AddRightBlock(toolbar, 0);
            EditorApplication.update += toolbar.Update;
        }
    }

    internal class ProjectSceneSelectorToolbarImpl : ToolbarBlock
    {
        private readonly Dropdown _dropdown;
        
        public ProjectSceneSelectorToolbarImpl()
        {
            var texts = new List<string>();
            _dropdown = new Dropdown(texts, 150);
            Elements.Add(_dropdown);

            UpdateScenes();
            
            _dropdown.ItemIsSelected.AddListener(SceneIsSelected);
            
            LoadState();
        }

        private void SceneIsSelected(int selectedIndex)
        {
            SaveState();
            var scenePaths = AssetUtils.GetAllScenePaths();
            EditorSceneManager.OpenScene(scenePaths[selectedIndex]);
        }

        private void UpdateScenes()
        {
            var scenePaths = AssetUtils.GetAllScenePaths();
            var sceneNames = scenePaths.Select(Path.GetFileNameWithoutExtension).ToList();
            _dropdown.SetItems(sceneNames);
            _dropdown.SelectItem(sceneNames.IndexOf(SceneManager.GetActiveScene().name));
            _dropdown.SetActive(Application.isPlaying == false);
        }

        public void Update()
        {
            try
            {
                UpdateScenes();
            }
            catch (Exception e)
            {
                // ignored
            }
        }
    }
}
