using System.Collections.Generic;
using System.Linq;
using Common.UI.Toolbar;
using Common.Utils;
using UnityEditor;
using UnityEngine;

namespace Common.Tools.ToolbarExtender
{
    public class ToolbarAreaExtender
    {
        protected readonly List<ToolbarBlock> Elements = new List<ToolbarBlock>();
        protected const float Space = 10;
        
        public void AddBlock(ToolbarBlock block, int position)
        {
            if (position > Elements.Count - 1)
            {
                var total = position - (Elements.Count - 1);
                for (var i = 0; i < total; i++)
                {
                    Elements.Add(null);
                }
            }
            Elements[position] = block;
        }

        protected virtual void DrawArea()
        {
            
        }
        
        public void Draw(Rect rect)
        {
            GUILayout.BeginArea(rect);
            GUILayout.BeginHorizontal();
            DrawArea();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
    }
}