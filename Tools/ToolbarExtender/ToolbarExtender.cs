using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Common.Tools.ToolbarExtender
{
	// https://github.com/marijnz/unity-toolbar-extender
	// For UNITY 2020.3.31f1
	[InitializeOnLoad]
	public static class ToolbarExtender
	{
		private static readonly ToolbarLeftAreaExtender LeftExtender = new ToolbarLeftAreaExtender();
		private static readonly ToolbarRightAreaExtender RightExtender = new ToolbarRightAreaExtender();
		
		public static readonly Action<Rect> LeftToolbarGUI = LeftExtender.Draw;
		public static readonly Action<Rect> RightToolbarGUI = RightExtender.Draw;

		public const float Space = 10;
		
		private const float SystemLeftToolbarWidth = 407;
		private const float SystemRightToolbarWidth = 373;
		private const float TopPadding = 5;
		private const float BottomPadding = 5;
		private const float PlayPauseStopButtonsLeftWidth = 69;
		private const float PlayPauseStopButtonsRightWidth = 25;

		static ToolbarExtender()
		{
			ToolbarCallback.OnToolbarGUI = OnGUI;
		}

		public static void AddLeftBlock(ToolbarBlock block, int position)
		{
			LeftExtender.AddBlock(block, position);
		}
		
		public static void AddRightBlock(ToolbarBlock block, int position)
		{
			RightExtender.AddBlock(block, position);
		}

		private static void OnGUI()
		{
			var screenWidth = EditorGUIUtility.currentViewWidth;

			var x = SystemLeftToolbarWidth + Space;
			var y = TopPadding;
			var width = screenWidth / 2 - x - PlayPauseStopButtonsLeftWidth - Space;
			var height = Screen.height - BottomPadding - TopPadding;
			var leftRect = new Rect(x, y, width, height);

			x = screenWidth / 2 + PlayPauseStopButtonsRightWidth + Space;
			width = screenWidth - x - SystemRightToolbarWidth - Space;
			var rightRect = new Rect(x, y, width, height);

			if (leftRect.width > 0)
			{
				LeftToolbarGUI.Invoke(leftRect);
			}

			if (rightRect.width > 0)
			{
				RightToolbarGUI.Invoke(rightRect);
			}
		}
	}
}
