using System.Collections.Generic;
using System.Linq;
using Common.UI.Toolbar;
using Common.Utils;
using UnityEditor;
using UnityEngine;

namespace Common.Tools.ToolbarExtender
{
    public class ToolbarRightAreaExtender : ToolbarAreaExtender
    {
        protected override void DrawArea()
        {
            if (Elements.IsEmpty()) return;
            
            Elements[0]?.Draw();
            for (var i = 1; i < Elements.Count; i++)
            {
                GUILayout.Space(Space);
                Elements[i]?.Draw();
            }
        }
    }
}