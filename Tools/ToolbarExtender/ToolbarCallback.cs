using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Common.Tools.ToolbarExtender
{
	public static class ToolbarCallback
	{
		private static readonly Type ToolbarType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.Toolbar");
		private static readonly Type GUIViewType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.GUIView");
		private static readonly Type WindowBackendType = typeof(UnityEditor.Editor).Assembly.GetType("UnityEditor.IWindowBackend");
		private static readonly PropertyInfo WindowBackend = GUIViewType.GetProperty("windowBackend", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
		private static readonly PropertyInfo ViewVisualTree = WindowBackendType.GetProperty("visualTree", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
		private static readonly FieldInfo IMGUIContainerOnGui = typeof(IMGUIContainer).GetField("m_OnGUIHandler", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
		private static ScriptableObject _currentToolbar;

		public static Action OnToolbarGUI;
		
		static ToolbarCallback()
		{
			EditorApplication.update -= OnUpdate;
			EditorApplication.update += OnUpdate;
		}

		private static void OnUpdate()
		{
			if (_currentToolbar != null) return;
			
			var toolbars = Resources.FindObjectsOfTypeAll(ToolbarType);
			_currentToolbar = toolbars.Length > 0 ? (ScriptableObject) toolbars[0] : null;
			
			if (_currentToolbar == null) return;
			
			var windowBackend = WindowBackend.GetValue(_currentToolbar);
			var visualTree = (VisualElement) ViewVisualTree.GetValue(windowBackend, null);
			var container = (IMGUIContainer) visualTree[0];

			var handler = (Action) IMGUIContainerOnGui.GetValue(container);
			handler -= OnGUI;
			handler += OnGUI;
			IMGUIContainerOnGui.SetValue(container, handler);
		}

		private static void OnGUI()
		{
			var handler = OnToolbarGUI;
			handler?.Invoke();
		}
	}
}
