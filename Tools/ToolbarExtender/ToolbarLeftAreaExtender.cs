using System.Collections.Generic;
using System.Linq;
using Common.UI.Toolbar;
using Common.Utils;
using UnityEditor;
using UnityEngine;

namespace Common.Tools.ToolbarExtender
{
    public class ToolbarLeftAreaExtender : ToolbarAreaExtender
    {
        protected override void DrawArea()
        {
            GUILayout.FlexibleSpace();
            if (Elements.IsEmpty()) return;
            
            Elements.Last()?.Draw();
            for (var i = Elements.Count - 2; i >= 0; i--)
            {
                GUILayout.Space(Space);
                Elements[i]?.Draw();
            }
        }
    }
}
