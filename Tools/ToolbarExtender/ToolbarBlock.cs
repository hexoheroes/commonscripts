using System;
using System.Collections.Generic;
using System.Linq;
using Common.UI.Toolbar;
using Unity.Plastic.Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace Common.Tools.ToolbarExtender
{
    public abstract class ToolbarBlock
    {
        protected readonly List<Element> Elements = new List<Element>();

        private const float SmallSpace = 1;

        public void Draw()
        {
            if (Elements.Count == 1)
            {
                Elements[0].Draw();
            }
            else if (Elements.Count > 1)
            {
                Elements[0].Draw();
                for (var i = 1; i < Elements.Count; i++)
                {
                    GUILayout.Space(SmallSpace);
                    Elements[i].Draw();
                }
            }
        }
        
        protected virtual void SaveWindowState(JArray array)
        {
        }
        
        protected void SaveState()
        {
            var isChanged = Elements.Any(el => el.IsChanged());
            if (!isChanged) return;
            
            var array = new JArray();
            SaveWindowState(array);
            Elements.ForEach(el => el.Save(array));
            EditorPrefs.SetString(GetType().Name, array.ToString());
        }
        
        protected virtual void LoadWindowState(JArray array)
        {
        }

        protected void LoadState()
        {
            try
            {
                var array = JArray.Parse(EditorPrefs.GetString(GetType().Name));
                LoadWindowState(array);
                Elements.ForEach(el => el.Load(array));
            }
            catch (Exception e)
            {
                // ignored
            }
        }
    }
}