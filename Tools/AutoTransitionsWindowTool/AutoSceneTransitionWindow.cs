using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Common.Scenes;
using Common.UI.EditorUI.Elements;
using Common.UI.EditorUI.Layouts;
using Common.UI.EditorUI.Windows;
using Common.Utils;
using Unity.Plastic.Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common.Tools.AutoTransitionsWindowTool
{
    public class AutoSceneTransitionWindow : DropdownWindow
    {
        private List<SceneTransition> _transitions;
        private List<int> _selectedTransitionIndexes = new List<int>();
        private List<Toggle> _toggles;

        public void SetTransitions(List<SceneTransition> transitions)
        {
            _transitions = transitions;
        }

        public List<int> GetSelectedTransitionIndexes()
        {
            return _selectedTransitionIndexes;
        }
        
        protected override void SaveWindowState(JArray array)
        {
            var arr = JArray.FromObject(_selectedTransitionIndexes);
            array.Add(arr);
            base.SaveWindowState(array);
        }

        protected override void LoadWindowState(JArray array)
        {
            if (array.Count > 0)
            {
                try
                {
                    _selectedTransitionIndexes = array[0].ToObject<List<int>>();
                    array.RemoveAt(0);
                }
                catch (Exception e)
                {
                    // ignored
                }
            }
            
            base.LoadWindowState(array);
        }
        
        protected override Layout InitElements()
        {
            _selectedTransitionIndexes = new List<int>();

            var verticalLayout = new VerticalLayout(300);
            
            _toggles = new List<Toggle>();
            for (var i = 0; i < _transitions.Count; i++)
            {
                var transition = _transitions[i];
                var toggle = new Toggle(transition.Name);
                _toggles.Add(toggle);
                var toggleIndex = i;
                toggle.Clicked.AddListener((isToggle) => SelectToggle(isToggle, toggleIndex));
                verticalLayout.AddElement(toggle);
            }

            ActivateDisableToggles();

            return verticalLayout;
        }

        private void SelectToggle(bool isToggle, int toggleIndex)
        {
            if (isToggle)
            {
                _selectedTransitionIndexes.Add(toggleIndex);
            }
            else
            {
                _selectedTransitionIndexes.RemoveAll(index => index == toggleIndex);
            }
            
            ActivateDisableToggles();
        }

        private void ActivateDisableToggles()
        {
            var scenePaths = AssetUtils.GetBuiltScenePaths();
            var sceneNames = scenePaths.Select(Path.GetFileNameWithoutExtension).ToList();
            List<int> targetTransitionIndexes;
            if (_selectedTransitionIndexes.IsEmpty())
            {
                var firstSceneName = sceneNames[0];
                var targetTransitions = _transitions.FindAll(trans => trans.From.name.Equals(firstSceneName)).ToList();
                targetTransitionIndexes = _transitions.IndexesOf(targetTransitions).ToList();
            }
            else
            {
                var previousTransition = _transitions[_selectedTransitionIndexes.Last()];
                var targetTransitions = _transitions.FindAll(trans => trans.From.name.Equals(previousTransition.To.name)).ToList();
                targetTransitionIndexes = _transitions.IndexesOf(targetTransitions).ToList();
            }
            
            for (var i = 0; i < _toggles.Count; i++)
            {
                if (_selectedTransitionIndexes.IsEmpty() == false && i == _selectedTransitionIndexes.Last())
                {
                    _toggles[i].SetActive(true);
                    continue;
                }

                var isActivated = targetTransitionIndexes.Contains(i) && _selectedTransitionIndexes.Contains(i) == false;
                foreach (var selectedTransitionIndex in _selectedTransitionIndexes)
                {
                    if (new FromToEqualityComparer().Equals(_transitions[selectedTransitionIndex], _transitions[i]))
                    {
                        isActivated = false;
                        break;
                    }
                }

                _toggles[i].SetActive(isActivated);
            }
        }
    }
}