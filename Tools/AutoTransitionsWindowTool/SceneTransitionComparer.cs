using System.Collections.Generic;

namespace Common.Tools.AutoTransitionsWindowTool
{
    public class FromToEqualityComparer : IEqualityComparer<SceneTransition>
    {
        public bool Equals(SceneTransition x, SceneTransition y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return Equals(x.From, y.From) && Equals(x.To, y.To);
        }

        public int GetHashCode(SceneTransition obj)
        {
            unchecked
            {
                return ((obj.From != null ? obj.From.GetHashCode() : 0) * 397) ^ (obj.To != null ? obj.To.GetHashCode() : 0);
            }
        }
    }
}
