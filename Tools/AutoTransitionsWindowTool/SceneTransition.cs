using System;
using UnityEditor;

namespace Common.Tools.AutoTransitionsWindowTool
{
    public class SceneTransition
    {
        private readonly string _name;
        public string Name => string.IsNullOrEmpty(_name) ? $"{From.name} -> {To.name}" : _name;
        public SceneAsset From { get; }
        public SceneAsset To { get; }
        public Func<bool> Action { get; }

        public SceneTransition(SceneAsset from, SceneAsset to, Func<bool> action)
        {
            From = from;
            To = to;
            Action = action;
        }
        
        public SceneTransition(string name, SceneAsset from, SceneAsset to, Func<bool> action) : this(from, to, action)
        {
            _name = name;
        }
    }
}
