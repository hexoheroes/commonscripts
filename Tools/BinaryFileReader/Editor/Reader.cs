using UnityEditor;
using UnityEngine;

namespace Common.Tools.BinaryFileReader.Editor
{
    public class Reader : EditorWindow
    {
        [MenuItem("ToolsBox/BinaryFileReader")]
        private static void Init()
        {
            System.Diagnostics.Process.Start(Application.dataPath + "/Scripts/Common/Tools/BinaryFileReader/BinaryFileReader.exe", $"p=\"{Application.persistentDataPath}\"");
        }
    }
}
