using System.IO;
using UnityEditor;
using UnityEngine;

namespace Common.Tools.TileImageManipulator.Editor
{
    public class ManipulatorContextMenu
    {
        [MenuItem("Assets/ToolsBox/TileImageManipulator", true)]
        private static bool TileImageManipulatorViewer()
        {
            return Selection.activeObject is Texture2D;
        }
      
        [MenuItem("Assets/ToolsBox/TileImageManipulator")]
        private static void TileImageManipulatorViewer(MenuCommand command)
        {
            var assetParentFolder = Directory.GetParent(Application.dataPath);
            var assetPath = Path.Combine(assetParentFolder.FullName, AssetDatabase.GetAssetPath(Selection.activeObject));
            System.Diagnostics.Process.Start(Application.dataPath + "/Scripts/Common/Tools/TileImageManipulator/GenerateTexture.exe", $"o=\"{assetPath}\"");
        }
    }
}
