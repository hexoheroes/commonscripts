using UnityEngine;
using UnityEditor;

namespace Common.Tools.TileImageManipulator.Editor
{
    public class ManipulatorMenu : EditorWindow
    {
        [MenuItem("ToolsBox/TileImageManipulator")]
        private static void Init()
        {
            System.Diagnostics.Process.Start(Application.dataPath + "/Scripts/Common/Tools/TileImageManipulator/GenerateTexture.exe", $"n=\"{Application.dataPath}\"");
        }
    }
}