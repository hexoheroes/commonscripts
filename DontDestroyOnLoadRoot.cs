using UnityEngine;

namespace Common
{
    public class DontDestroyOnLoadRoot : MonoBehaviour
    {
        public static DontDestroyOnLoadRoot Instance;

        public void Awake()
        {
            if (Instance)
            {
                MoveChildren(this);
                Destroy(this);
            }
            else
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }

        public void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }

        private void MoveChildren(DontDestroyOnLoadRoot from)
        {
            foreach (Transform child in from.transform)
            {
                child.parent = Instance.transform;
            }
            // var children = from.transform.GetComponentsInChildren<Transform>().ToList();
            // children.Remove(transform);
            // foreach (var child in children)
            // {
            //     child.parent = Instance.transform;
            // }
        }
    }
}
